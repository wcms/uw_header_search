/**
 * @file
 */

(function ($) {
    Drupal.theme.prototype.CToolsThrobber = function () {
        var html = '';
        html += '  <div id="modal-throbber">';
        html += '    <div class="modal-throbber-wrapper">';
        html += '     <div class="cssload-loader">';
        html += '       <div class="cssload-flipper">';
        html += '         <div class="cssload-front"></div>';
        html += '         <div class="cssload-back"></div>';
        html += '       </div>';
        html += '     </div>';
        html += Drupal.CTools.Modal.currentSettings.throbber;
        html += '    </div>';
        html += '  </div>';

        return html;
    };
})(jQuery);
